/**
 * Created by sancez on 26.04.17.
 */
angular.module('shopApp', [
     'ui.bootstrap',
    'angularFileUpload',
    'templates-module',
    'infinite-scroll',
    'toastr',


    'ngAnimate',
    'ngTouch',
    'ui.router',

    'df.common'
])
    .factory('df.webService',[ '$http', '$location', function ($http, $location) {
        return {
            getData: function (method, path) {
                var r = $location.protocol() + '://' + $location.host();
                if($location.port()){
                    r = r + ':' +  $location.port();
                }
                return function (data) {
                    return $http[method](r +path, data)
                        .then(function(response) {
                            return response.data;
                        });
                }
            }
        }
    }])
    .config(['$stateProvider', '$qProvider', '$urlRouterProvider', '$locationProvider',
        function ($stateProvider, $qProvider, $urlRouterProvider, $locationProvider) {
        $qProvider.errorOnUnhandledRejections(false);
        $stateProvider
            .state('main', {
                url: '/?flt',
                templateUrl: 'application/mainPage/mainPage.tpl.html',
                controller: 'df.mainCtrl',
                reloadOnSearch: false,
                data:{}
            })
            .state('login', {
                url: '/admin',
                templateUrl: 'application/admin/login.tpl.html',
                controller: 'df.admin',
                data:{}
            })
            .state('orders', {
                url: '/orders',
                templateUrl: 'application/admin/orders.tpl.html',
                controller: 'df.orders',
                data: {}
            });
        $urlRouterProvider.otherwise( '/' ); //  /asset/search/rent

        // prerender
        $locationProvider.hashPrefix('!');
        $locationProvider.html5Mode({enabled : true, requireBase : true});

    }])
    .run(['$rootScope', '$q', '$locale', 'df.webService', 'toastr', '$state', '$uibModal', '$location',
        function ($rootScope, $q, $locale, webService, toastr, $state, $uibModal, $location) {

        $locale.NUMBER_FORMATS.GROUP_SEP = '\u00a0';

        $rootScope.gas = function() {
            var u = ($rootScope.user || {}).isAuthenticated;
            if (window.ga && !u)
                window.ga('send', 'pageview', $location.url());
        };

        $rootScope.gasEvent = function(elm,eventType,label) {
            var u = ($rootScope.user || {}).isAuthenticated;
            if (window.ga && !u)
                ga('send', 'event', elm, eventType, label);
        };

        $rootScope.back = function() {
            window.history.back();
        };

        $rootScope.commonModal = function(template, size, model, scope, windowClass) {
            var opts = {
                templateUrl: template,
                controller: 'df.commonModalCtrl',
                windowClass: windowClass || 'modal',
                size: size,
                resolve: {
                    model: function () {
                        return model;
                    }
                }
            };
            if (scope)
                opts.scope = scope;
            return $uibModal
                .open(opts)
                .result;
        };

        var logOut = webService.getData('post', '/services/logout');
        $rootScope.logOut = function () {
            logOut({})
                .then(function (e) {
                    $state.go('main');
                    $rootScope.user.isAuthenticated = !$rootScope.user.isAuthenticated;
                    $rootScope.showToastr('success', {
                        title: 'Успешно вышли',
                        iconFaClass: 'fa-check'
                    });
                })
        };

        $rootScope.toLogIn = function () {
            $state.go('login');
        };

        $rootScope.showToastr= function (type, attrs) {
            // title, msg, params, promise)
            attrs.params = attrs.params || {};
            attrs.iconFaClass = attrs.iconFaClass || 'fa-check';
            var t = toastr[type](attrs.msg,attrs.title, attrs.params);
            t.scope.iconFaClass = 'fa fa-2x ' + attrs.iconFaClass;

            if(!attrs.promise)  return;

            return	attrs.promise
                .then(function () {
                    toastr.clear(t);
                })
        };

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, error) {
            var ttttt = 1;
            $rootScope.gas();
        });
        $rootScope.$on('$stateChangeError',
            function(event, toState, toParams, fromState, fromParams, error){
                if (error)
                    $log.error(error.data);
            });
        var isAuth = webService.getData('post', '/services/isAuth');
        isAuth({})
            .then(function (e) {
                $rootScope.user = e;
            });


    }])
    .controller('appCtrl',['$scope', 'df.webService', 'FileUploader', '$state' ,function ($scope, webService, FileUploader, $state) {

    }])
    .controller('df.commonModalCtrl', ['$rootScope', '$scope', '$uibModalInstance', 'model', '$timeout', 'df.webService', function ($root, $scope, $uibModalInstance, model, $timeout, webService) {
        var addOrder = webService.getData('post', '/services/addOrder');
        $scope.model = model;
        $scope.recipientInfo = {

        };
        $scope.ok = function () {
            if (!$scope.recipientInfo.phone) {
                $root.showToastr('error', {
                    title: 'Ошибка',
                    msg: 'Введите номер телефона!',
                    iconFaClass: 'fa-exclamation-circle'
                })
            } else {
                addOrder({ recipientInfo: $scope.recipientInfo, product: $scope.model });
                $uibModalInstance.close(model);
            }
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('df.mainCtrl', ['$rootScope', '$scope', 'df.webService', '$state', '$location', 'vsil.convertUtil', '$q', function ($root, $scope, webService, $state, $location, convertUtil,$q) {
        var getProducts = webService.getData('post', '/services/getProducts');
        var itemCount = 3;
        $scope.list = [];
        $scope.allListDnl = false;
        $scope.pageCount = 0;
        $scope.vm = {};
        $scope.vm.filter = {
            type: 'box',
            collection: 'White'
        };

        var locationToFilter = function () {
            var tFilter = $location.search().flt;
            var filterw = convertUtil.qs.parse(tFilter);
            $scope.vm.filter = filterw;
        };

        $scope.setFilter = function (collection, type) {
            if(type)
                $scope.vm.filter.type = type;

            if(collection)
                $scope.vm.filter.collection = collection;

            filterToLocation();
            //$scope.clearItems();
            //$scope.getItems();
        };
        $scope.clearItems = function () {
            $scope.list = [];
            $scope.pageCount = 0;
            $scope.allListDnl = false;
            itemCount = 3;
        };

        $scope.switchSize = function (item, size, count) {
            if (item[size]) {
                item.price = item[size];
                item.selectedBox = size;
                item.count = item[count];
            }
        };
        var filterToLocation = function () {
            var r = convertUtil.qs.stringify($scope.vm.filter);
            $location.search('flt', r);
            $scope.f = angular.extend({},$scope.vm.filter);
            if(!$scope.vm.filter.type){
                $scope.f.type = 'box';
                $scope.vm.filter.type = 'box';
            }
            if(!$scope.vm.filter.collection){
                $scope.vm.filter.collection = 'White';
                $scope.f.collection = 'White';
            }
        }

        $scope.getItems = function (flag) {
            if(!$scope.allListDnl || flag){
                filterToLocation();
                $scope.pageCount++;
                getProducts({
                    filter: $scope.f,
                    page:{
                        index: $scope.pageCount,
                        count: itemCount
                    }
                })
                    .then(function (e) {
                        $scope.listCount = e.count;
                        if(e.items.length < itemCount || e.items.length == 0){
                            $scope.allListDnl = true;
                        }
                        if(e.items.length > 0){
                            angular.forEach(e.items, function (i) {
                                $scope.list.push(i);
                            });
                        }
                    });
            }
        };

        $scope.$on('$locationChangeSuccess',
            function(){
                loadLoc();
            });

        var loadLoc = function () {
            $q.when()
                .then(function () {
                    locationToFilter();
                })
                .then(function () {
                    $scope.clearItems();
                    $scope.getItems(true);
                })
        };
        loadLoc();
    }])
    .controller('df.admin', [ '$scope', '$state', 'df.webService', function ($scope, $state, webService) {
        var logIn = webService.getData('post', '/services/login');
        $scope.vm = {

        };
        $scope.logIn = function () {
            logIn({
                username: $scope.vm.name,
                password: $scope.vm.password
            })
                .then(function (e) {
                if(!e || e.error){
                    $scope.$root.showToastr('error', {
                        title: 'Ошибка',
                        msg: 'Не удалось аутендифицировать',
                        iconFaClass: 'fa-exclamation-circle'
                    });
                    $scope.$root.user.isAuthenticated = false;
                    return;
                }else{
                    $scope.$root.user.isAuthenticated = true;
                    $scope.$root.showToastr('success', {
                        title: 'Вход выполнен',
                        iconFaClass: 'fa-check'
                    });
                    $state.go('main');
                }
            }).catch(function (e) {
                if (e.data === "Unauthorized") {
                    $scope.$root.showToastr('error', {
                        title: 'Ошибка',
                        msg: 'Проверьте правильность ввода логина и пароля!',
                        iconFaClass: 'fa-exclamation-circle'
                    });
                    $scope.$root.user.isAuthenticated = false;
                    return;
                } else {
                    $scope.$root.showToastr('error', {
                        title: 'Ошибка',
                        msg: 'Что-то пошло не так!',
                        iconFaClass: 'fa-exclamation-circle'
                    })
                }
            })
        };

    }])
    .controller('df.orders', ['$scope', '$state', 'df.webService', function ($scope, $state, webService) {
        var orders = webService.getData('post', '/services/getOrders');
        var isAuth = webService.getData('post', '/services/isAuth');
        $scope.isAdmin;
        $scope.orders = [];
        $scope.vm = {};
        $scope.getOrders = function () {
            orders({
                filter: $scope.filter || undefined,
                page:{
                    index: $scope.vm.pageCount,
                    count: 30
                }
            }).then(function (e) {
                $scope.orders = e;
                !$scope.count ?  $scope.count = e[0].order.number : undefined ;
            })
        };
        isAuth({}).then(function (e) {
            if (e.isAuthenticated) {
                $scope.getOrders();
            } else {
                $scope.isAdmin = e.isAuthenticated;
            }
        })
    }])
    .directive('dfMainList', [ 'df.webService', '$state', 'FileUploader',
        function(webService, $state, FileUploader) {
        return {
            templateUrl: 'application/list/mainList.tpl.html',
            scope:{
                model: '=dfMainList',
            },
            controller: 'df.mainCtrl',
            link: function($scope, elm, attrs, dropdown) {

                var changeStatus = webService.getData('post', '/services/addProduct');

                $scope.openModal = function ($event,item) {
                    if (item.price) {
                        $scope.getOrder(item);
                    } else {
                        $event.preventDefault();
                        $event.stopPropagation();
                        console.log(item);
                        item.status = !item.status;
                    }
                }

                $scope.changeStatus = function (item) {
                        item.isDeleted = !item.isDeleted;
                        changeStatus({ product: item });
                };

                var uploader = $scope.uploader = new FileUploader({
                    url: '/image/upload',
                    autoUpload: true
                });
                // $scope.progress = uploader.progress;
                uploader.onBeforeUploadItem = function(item) {
                    item.formData.push();
                };
                uploader.onSuccessItem = function(e) {
                    $scope.vm.addedItem.images.push(angular.fromJson(e._xhr.response).id);
                    $scope.$root.showToastr('success', {
                        title: 'Сохранено',
                        iconFaClass: 'fa-check'
                    });
                    uploader.clearQueue();
                };

                uploader.filters.push({
                    name: 'imageFilter',
                    fn: function(item) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if(item.size >= 30000000){
                            $scope.$root.showToastr('error', {
                                title: 'Ошибка',
                                msg: 'Файл слишком большой',
                                iconFaClass: 'fa-exclamation-circle'
                            });
                            return false;
                        }else{
                            if('|jpg|png|jpeg|bmp|gif|'.indexOf(type) === -1) {
                                $scope.$root.showToastr('error', {
                                    title: 'Ошибка',
                                    msg: 'Файл не является картинкой',
                                    iconFaClass: 'fa-exclamation-circle'
                                });
                                return false;
                            }else{
                                return true;
                            }
                        }
                    }
                });

                var addProduct = webService.getData('post', '/services/addProduct');
                $scope.vm = {
                    addedItem: {
                        type: ($state.params ||{}).type,
                        descriptions: [],
                        images: []
                    },
                    edit: {
                        collections: ['White', 'Black', 'Exclusive']
                    },
                    addingStep: 0
                };

                $scope.nextImg = function (item) {
                    if(item.activeImg == item.images.length - 1){
                        item.activeImg = 0;
                        return;
                    }
                    item.activeImg++;
                };
                $scope.prevImg = function (item) {
                    if(item.activeImg == 0){
                        item.activeImg = item.images.length - 1;
                        return;
                    }
                    item.activeImg--;
                };

                $scope.getOrder = function (item) {
                    $scope.$root.commonModal('application/modal/orderCommonModal.tpl.html','md',item,$scope)
                        .then(function (e) {
                            console.log(e);
                        })
                }

                $scope.nextStep = function (int) {
                    var r = $scope.vm.addedItem;
                    if(int == 1){
                        if(r.collection && r.name  && r.flowerType && r.flowerCnt){
                            $scope.vm.addingStep = 1;
                        }else{
                            $scope.$root.showToastr('error', {
                                title: 'Ошибка',
                                msg: 'Не все поля введены',
                                iconFaClass: 'fa-exclamation-circle'
                            });
                        }
                    }
                    if(int == 2){
                        if(r.priceM || r.priceS|| r.priceL || r.priceXl){
                            $scope.vm.addingStep = 2;
                        }else{
                            $scope.$root.showToastr('error', {
                                title: 'Ошибка',
                                msg: 'Не все поля введены',
                                iconFaClass: 'fa-exclamation-circle'
                            });
                        }
                    }
                }
                $scope.saveItem = function () {
                    if($scope.vm.addedItem.images.length < 0){
                        $scope.$root.showToastr('error', {
                            title: 'Ошибка',
                            msg: 'Не добавленны фото',
                            iconFaClass: 'fa-exclamation-circle'
                        });
                        return;
                    }
                    $scope.vm.addedItem.isDeleted = false;
                    addProduct({product:$scope.vm.addedItem})
                        .then(function (e) {
                            if(!e || e.error || !e.result){
                                $scope.$root.showToastr('error', {
                                    title: 'Ошибка',
                                    msg: 'Товар не сохранен',
                                    iconFaClass: 'fa-exclamation-circle'
                                });
                                return;
                            }
                            $scope.$root.showToastr('success', {
                                title: 'Сохранено',
                                iconFaClass: 'fa-check'
                            });
                            $scope.model.unshift(e.result);
                            $scope.vm.addedItem = {
                                type: ($state.params ||{}).type || "box",
                                    descriptions: [],
                                    images: []
                            };
                            $scope.vm.addingStep = 0;

                        })
                };
                $scope.addDescItem = function () {
                    $scope.vm.addedItem.descriptions.push({value:'',key:''})
                }
            }
        };
    }])
    .factory('vsil.convertUtil', ['$log', function($log){

        var separator1 = ',',
            separator2 = '_',
            strSep = '.',
            defSeparatirs = ',._';

        var queryToNode = function(root, value, separators) {
            if (!value) {
                angular.copy({}, root);
                return;
            }
            var sprs = separators.split(strSep),
                spr = sprs.splice(0, 1)[0],
                items = value.split(spr);
            angular.forEach(items, function(e) {
                queryToNodeItem(root, e, sprs);
            });
        };

        var nodeToQuery = function(root, key) {
            if (!root || angular.isFunction(root))
                return;
            var result = [ key ];
            angular.forEach(root, function(v, k) {
                var items = nodeToQuery(v, k);
                result.push(items);
            });
            return result;
        };

        var queryToNodeItem = function(root, value, separators) {
            var spr = separators[0],
                items = value.split(spr),
                key = items.splice(0, 1)[0];
            if (items.length == 0) {
                root[key] = true;
                return;
            }
            root[key] = {};
            angular.forEach(items, function(e) {
                queryToNode(root[key], e, separators.join(strSep));
            });
        };

        var getSetListValue = function(items, keyGetter, value) {
            var hash,
                result = [];
            if (angular.isDefined(value)) {
                hash = {};
                for (var i = 0; i < value.length; i++)
                    hash[value[i]] = true;
            }
            for (var i = 0; i < items.length; i++) {
                var item = this.items[i],
                    k = keyGetter ? keyGetter(item.item) : item.item;
                if (hash)
                    item.isSelected = !!hash[k];
                result.push(k);
            }
            return result;
        };

        var self = {
            toSelectableList: function (items, keyGetter) {
                var result = {
                    items: [],
                    value: function (value) {
                        return getSetListValue(this.items, keyGetter, value);
                    }
                };
                for (var i = 0; i < items.length; i++)
                    result.items.push({
                        isSelected: false,
                        item: items[i]
                    });
                return result;
            },

            range: {
                toQuery: function(value, builder) {
                    if (!value)
                        return;
                    var min = value.min && (!value.bounds || value.bounds.min < value.min) ?
                            value.min : null,
                        max = value.max && (!value.bounds || value.bounds.max > value.max) ?
                            value.max : null;
                    if (!(min || max))
                        return;
                    var result = [
                        min,
                        max
                    ];
                    if (builder)
                        builder(result, value);
                    return result.join();
                },
                fromQuery: function(value, builder) {
                    if (!value)
                        return;
                    var r = value ? value.split(separator1) : [];
                    var result = {
                        min: r[0],
                        max: r[1]
                    };
                    if (builder)
                        builder(result, r);
                    return result;
                }
            },
            keyList: {
                toQuery: function(value) {
                    var items = [];
                    angular.forEach(value, function(v, k) {
                        var item = nodeToQuery(v, k);
                        if (item)
                            items.push(item.join(separator2));
                    });
                    if (items.length == 0)
                        return;
                    return items.join(separator1);

                },
                fromQuery: function(value) {
                    if (!value)
                        return;
                    var result = {};
                    queryToNode(result, value, defSeparatirs);
                    return result;
                }
            },
            date: {
                toQuery: function(value) {
                    if (value)
                        return [
                            value.getFullYear(),
                            value.getMonth(),
                            value.getDate()
                        ].join();
                },
                fromQuery: function(value) {
                    if (!value)
                        return;
                    var v = value.split(separator1);
                    return new Date(v[0], v[1], v[2])
                }
            },
            itemId: {
                toQuery: function(value) {
                    if (value)
                        return value.id;
                },
                fromQuery: function(value) {
                    if (value)
                        return { id: value };
                }
            },

            qs: {
                stringify: function(obj, prefix, target){
                    var strs = target || [],
                        bools = [];
                    angular.forEach(obj, function(v, key) {
                        var k = prefix ? prefix + '.' + key : key;
                        if (angular.isArray(v)) {
                            v = v.join(',');
                            k = k + ',';
                        }
                        else if (angular.isNumber(v))
                            k = k + '-';
                        else if(angular.isDate(v)) {
                            k = k + '-d';
                            v = self.date.toQuery(v);
                        }
                        if (angular.isObject(v))
                            self.qs.stringify(v, k, strs);
                        else if (v === true)
                            bools.push(key);
                        else if (v)
                            strs.push(k + ":" + v);
                    });
                    if (bools.length > 0)
                        strs.push((prefix || '') + '-b:' + bools.join(','));
                    return strs;
                },
                parse: function(values, target){
                    var obj = target || {};
                    if (!values)
                        return obj;
                    var	value = angular.isArray(values) ? values : [ values ];
                    for (var i = 0; i < value.length; i++) {
                        var item = value[i];
                        var ix = item.indexOf(':'),
                            names = item.substring(0, ix).split('.'),
                            v = item.substring(ix + 1),
                            iObj = obj;
                        for (var j = 0; j < names.length - 1; j++)
                            iObj = iObj[names[j]] = iObj[names[j]] || {};
                        var n = names[names.length - 1];
                        if (n[n.length - 1] === ',' && v)
                            iObj[n.substring(0, n.length - 1)] = v.split(',');
                        else if (n[n.length - 1] === '-' && v)
                            iObj[n.substring(0, n.length - 1)] = parseFloat(v);
                        else if (n.substring(n.length - 2) === '-b' && v) {
                            var bN = n.substring(0, n.length - 2),
                                bObj = bN ? (iObj[bN] = iObj[bN] || {}) : iObj,
                                bAr = v.split(',');
                            for (var j = 0; j < bAr.length; j++) {
                                bObj[bAr[j]] = true;
                            }
                        }
                        else if (n.substring(n.length - 2) === '-d' && v) {
                            var dN = n.substring(0, n.length - 2);
                            iObj[dN] = self.date.fromQuery(v);
                        }
                        else if (v)
                            iObj[n] = v;
                    }
                    return obj;
                }
            }
        };

        return self;
    }])

    .directive('dfEdit', [ 'df.webService', '$state', 'FileUploader',
        function (webService, $state, FileUploader) {
        return {
            templateUrl: 'application/productEditor/productEditor.tpl.html',
            scope: {
                model: '=dfEdit'
            },
            link: function ($scope) {
                console.log($scope.model);
                var save = webService.getData('post', '/services/addProduct');
                $scope.alert = function (smth) {
                    console.log(smth);
                }
                $scope.close = function (item) {
                    item.edit = !item.edit;
                    save({product: item})
                        .then(function(e) {
                        if (e.result) {
                            $scope.$root.showToastr('success', {
                                title: 'Изменения сохранены!',
                                iconFaClass: 'fa-exclamation-circle'
                            });
                        } else {
                            $scope.$root.showToastr('error', {
                                title: 'Что-то пошло не так!',
                                iconFaClass: 'fa-exclamation-circle'
                            });
                        }
                    });
                };

                $scope.nextStep = function (int) {
                    if(int == 1){
                        if( $scope.model.collection &&
                            $scope.model.name  &&
                            $scope.model.flowerType &&
                            $scope.model.flowerCnt
                        ){
                            $scope.vm.editingStep = 1;
                        } else {
                            $scope.$root.showToastr('error', {
                                title: 'Ошибка',
                                msg: 'Не все поля введены',
                                iconFaClass: 'fa-exclamation-circle'
                            });
                        }
                    }
                    if(int == 2){
                        if( $scope.model.priceM ||
                            $scope.model.priceS ||
                            $scope.model.priceL ||
                            $scope.model.priceXl
                        ){
                            $scope.vm.addingStep = 2;
                        }else{
                            $scope.$root.showToastr('error', {
                                title: 'Ошибка',
                                msg: 'Не все поля введены',
                                iconFaClass: 'fa-exclamation-circle'
                            });
                        }
                    }
                }

                $scope.vm = {
                    edit: {
                        collections: ['White', 'Black', 'Exclusive']
                    },
                    editingStep: 0
                };
            }
        };
    }])
;