// ======================   Подключение сторонних модулей   ======================

var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    fs = require('fs');

// ======================   Глобальные переменный   ======================

var users = [{id: 1, username: 'admin', password: '12345'}];


passport.use(new LocalStrategy({passReqToCallBack: true}, function (username, password, done) {
        findUser(username, function (err, user) {
            if (err) {
                return done(err);
            }

            if (!user) {
                return done(null, false, {message: 'The user is missing'});
            }

            if(user.password != password) {
                return done(null, false, {message: 'Incorrect password!'});
            }

            return done(null, user, {message: 'true'});
        })
    }
));

function findUser (username, cb) {
    process.nextTick(function () {
        for (var i = 0, len = users.length; i < len; i++) {
            var user = users[i];
            if (user.username === username) {
                return cb(null, user);
            }
        }

        return cb(null, null);
    });
}

function findById (id, cb) {
    process.nextTick(function () {
        var idx = id - 1;
        if (users[idx]) {
            return cb(null, users[idx]);
        } else {
            return cb(null, null);
        }
    });
}

passport.serializeUser(function (user, cb) {
    return cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
    findById(id, function (err, user) {
        if (err) {
            return cb(err);
        }

        cb(null, user);
    })
});

module.exports = passport;

