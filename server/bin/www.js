var serverApp = require('../index.js');

serverApp
    .then(function (app) {
        app.listen(3000, function () {
            console.log('Example app listening on port 3000!');
        });
    })