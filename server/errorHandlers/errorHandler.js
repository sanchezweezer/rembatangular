var logger = require('../logger/logging.js');


var uErrorHandler = function (err, req, res, next) {
    logger.log('error', 'Error code: ' + err.status, 'Stack: ' + err.stack);
    console.log('error', 'Error code: ' + err.status, 'Stack: ' + err.stack);

    if (err.status == 404) {
        res.send('Not found!');
    } else if (err.status == 500) {
        res.send('Internal error!');
    }
};
module.exports = uErrorHandler;