var path = require('path'),
    winston = require('winston');

var logger = new winston.Logger({
    level: 'error',
    transports: [
        new (winston.transports.File)({ filename: path.join(__dirname,'../logs/error.log') })
    ]
});

module.exports = logger;