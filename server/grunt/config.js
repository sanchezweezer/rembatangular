/**
 * Created by Дмитрий on 11.05.2017.
 */
module.exports = {
    clean: {
        options: {
            force: true
        },
        build:   [ '../../client/release/build/*' ]
    },
    less: {
        development: {
            files: {
                '../../client/release/build/main_0.0.1.css': '../../client/styles/less/main.less',
                // '../../client/release/build/vendors/bootstrap.min.css': '../../client/bower_components/bootstrap/less/bootstrap.less'
            }
        },
        prod: {
            files: {
                '../../client/release/build/main_0.0.1.css': '../../client/styles/less/main.less'
            },
            options: {
                cleancss: true,
                compress: true
            }
        }
    },
    concat: {
        js_compile_default: {
            src: [ '../../client/app/**/*.js'],
            dest: '../../client/release/build/js/spa.js'
        }
    },
    copy:{
        index: {
                nonull: true,
                cwd: '../../client/app/',
                src: [
                    'index.html'
                ],
                dest: '../../client/release/build/',
                expand: true,
                flatten: false
            },
        files: {
                nonull: true,
                cwd: '../../client/app/',
                src: [
                    '**/*.js'
                ],
                dest: '../../client/release/build/js',
                expand: true,
                flatten: false
            },
        vendors: {
                nonull: true,
                cwd: '../../client/bower_components/',
                src: [
                    'angular/angular.min.js',
                    'angular/angular.min.js.map',

                    'angular-touch/angular-touch.min.js',
                    'angular-touch/angular-touch.min.js.map',

                    'angular-bootstrap/ui-bootstrap-tpls.min.js',

                    'angular-animate/angular-animate.min.js',
                    'angular-animate/angular-animate.min.js.map',

                    'angular-file-upload/dist/angular-file-upload.js',
                    'angular-file-upload/dist/angular-file-upload.js.map',

                    'angular-toastr/dist/angular-toastr.tpls.min.js',

                    'angular-ui-router/release/angular-ui-router.js',

                    'ngInfiniteScroll/build/ng-infinite-scroll.min.js',

                     'bootstrap/dist/css/bootstrap.min.css',
                     'bootstrap/dist/css/bootstrap.min.css.map'
                ],
                dest: '../../client/release/build/vendors/',
                expand: true,
                flatten: true
        }
    },
    less_imports: {
        options: {
            inlineCSS: false // default: true
        },
        '../../client/styles/less/int/import.less': [ '../../client/styles/less/int/blocks/**/*.less' ]
    },
    html2js: {
        options: {
            indentString: ' ',
            quoteChar: '\'',
            htmlmin: {
                collapseWhitespace: true,
                removeComments: true,
                processScripts: [ 'text/ng-template' ],
                keepClosingSlash: true
            },
            base: '../../client/app/'
        },
        module:{
            src: '../../client/app/**/*.tpl.html',
            dest: '../../client/release/build/templatesApp.js'
        }
    },
    watch: {
        options: {
            livereload: true
        },
        html: {
            files: ['../../client/app/**/*.html'],
            tasks: ['html2js', 'copy:files', 'copy:index']
        },
        js: {
            files: ['../../client/app/**/*.js'],
            tasks: ['copy:files']
        },
        less: {
            files: ['../../client/styles/less/int/blocks/**/*.less', '../../client/styles/less/variables.less'],
            tasks: ['less_imports', 'less:development']
        }
    }
};