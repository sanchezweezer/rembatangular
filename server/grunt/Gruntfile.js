/**
 * Created by Дмитрий on 11.05.2017.
 */
var config = require('./config.js');


module.exports = function (grunt) {
    grunt.initConfig(config);

    grunt.file.defaultEncoding = 'utf8';
    grunt.file.preserveBOM = true;

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-less-imports');
    grunt.registerTask( 'delta', [ 'clean', 'html2js', 'copy', 'less_imports', 'less:development', 'watch' ] );
    grunt.registerTask( 'release-on-prod', [ 'clean', 'html2js', 'concat:js_compile_default','copy:index',
        'copy:vendors', 'less_imports', 'less:prod' ] );
};
