/**
 * Created by sergeycherepanov on 25.05.17.
 */

var q = require('q');
var assert = require('assert');
var configVariables = require('../config/configVariables');
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
//var url = 'mongodb://localhost:27017/dev';
//var urlMongo = 'mongodb://DBuserProject:Yamaha700@92.243.95.51:27017/project';
var urlMongo = 'mongodb://' + configVariables.mondgo.users.product + ':'
                + configVariables.mondgo.password.product + '@'
                + configVariables.mondgo.url.server + '/'
                + configVariables.mondgo.db.product;

var database;

var dbClass = {
    connect: function () {
        var deferred = q.defer();
        MongoClient.connect(urlMongo, function (err, db) {
            assert.equal(null, err);
            database = db;
            console.log('BD OK!');
            deferred.resolve();
            return err;

        });
        return deferred.promise
    },

        // зпрос всей коллекции
        // входные параметры: ColName - имя коллекции, filter -  фильтр (json с нужными параметрами поиска)
        //                    startTo - с какой позиции отдавать товары
        //                    count - кол-во товаров
        // выходные параметры: промис с массивом json-нов
    ShowCollection: function(ColName, filter, startTo, count){
        if(startTo === undefined)
            startTo = 0;
        if(count  === undefined)
            count = 0;
        var deferred = q.defer();
        var collection = database.collection(ColName);
        collection.find(filter).skip(startTo).limit(count).sort({ $natural: -1 }).toArray(function (err, result) {
            if (!err) {
                //console.log(database);
                deferred.resolve(result);

            }
            else
                deferred.reject(err);
        });
        return deferred.promise;
    },

        // Добаление документа в коллекцию товаров
        // Входные параметры:   ColName: имя коллекции
        //                      product: json c товаром
        // Выходные парметры: промис с результатом операции
    InsertCollection: function (ColName, product) {

        if(ColName === undefined)
        {
            return {msg: 'Не передано имя коллекции'};
        }
        
        //if(!check(product))
            //return {msg: 'JSON is not valid'};

        var collection = database.collection(ColName);

        return collection.insertOne(product);
    },

    ImagesCount: function () {
        var collection =  database.collection(configVariables.mondgo.collections.countImages);
        return collection.findOne();
    },

    ImagesCountIncrement: function (count) {
        var collection =  database.collection(configVariables.mondgo.collections.countImages);
        var countNew = count + 1;
        collection.update({count: count}, {count: countNew});
    },

        // Кол-во товаров
        // Если передан параметр product, то вернёт кол-во товаров в соответствие с фильтром product
        // Если параметр product не передан, то вернёт общее кол-во товаров
    ProductCount: function (product) {
        var collection =  database.collection(configVariables.mondgo.collections.product);
        return collection.find(product).count();
    },

    OrdersCount: function () {
        var collection = database.collection(configVariables.mondgo.collections.orders);
        return collection.count();
    },

        // Изменние статуса isDeleted в бд
        // Входные параметры: itemId: json c параметрами для поиска (в общем случае достаточно Id)
        //                              isDeleted: новое значение флага удаления типа boolean.
        // Выходные параметры: промис с результатом
        // Важно: валидность и правильность входного json должно проверяться на сервере
        //        на стороне логики работы с бд это проверять не возможно, т.к. заранее
        //        не известен набор входных полей для поиска
    DeleteItem: function (itemId, isDeleted) {

        if (isDeleted === undefined)
            return {msg: 'Не передан обязательный параметр isDeleted!'};
        if (typeof (isDeleted) !== 'boolean')
            return {msg: 'Переданый параметр не типа boolean!'};
        if (!itemId)
            return {msg: 'Не передан item!'};


        var collection = database.collection(configVariables.mondgo.collections.product);

        return collection.update(itemId, {$set: {isDeleted: isDeleted}});
    },

        // Обновление товара
        // Входные параметры: product: json c товаром
        // Выходные параметры: промис с результатом
    ProductUpdate: function (product) {
        //if(!check(product))
            //return {msg: 'JSON is not valid'};

        var collection = database.collection(configVariables.mondgo.collections.product);
        // product._id = new ObjectID.createFromHexString(product._id);
        delete product._id;
        return collection.update({Id: product.Id}, product);
    }

};

var check = function (product) {
    if( product.Id &&
        product.name &&
        product.price &&
        product.Type &&
        product.Description &&
        product.Images &&
        product.Date &&
        product.isDeleted !== undefined
    ) {
        return true
    }
    else
        return false
};

module.exports = dbClass;

