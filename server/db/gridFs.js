
var mongo = require('mongodb'),
    mongoose = require('mongoose'),
    fs = require('fs'),
    path = require('path'),
    q = require('q'),
    multiparty = require('multiparty'),
    gm = require('gm').subClass({imageMagick: true}),
    Grid = require('gridfs-stream');
    mongoose.Promise = require('q').Promise;

var configVariables = require('../config/configVariables');

var dbClass = require('./mongoDb');

//var urlGrid = 'mongodb://DBuserTest:Yamaha700@92.243.95.51:27017/test';
// var urlGrid = 'mongodb://' + configVariables.mondgo.users.images + ':'
//     + configVariables.mondgo.password.images + '@'
//     + configVariables.mondgo.url.server + '/'
//     + configVariables.mondgo.db.gridFs;


var urlGrid = 'mongodb://' + configVariables.mondgo.url.server + '/'
              + configVariables.mondgo.db.gridFs;
var options = {
    user: configVariables.mondgo.users.images,
    pass: configVariables.mondgo.password.images
};
var conn, gfs;


var selfM;
module.exports = selfM = {
    connect: function () {
        return mongoose.connect(urlGrid, options)
            .then(function () {
                selfM.configGridFs()
            })
    },
    configGridFs: function () {
        conn = mongoose.connection;
        Grid.mongo = mongoose.mongo;
        gfs = Grid(conn.db);
        gfs.collection('ctFiles'); //set collection name to slookup into
        gfs.files.ensureIndex({'metadata.id':1}, {sparse : true});
        console.log('gridFs ok!');
    },
    uploadImg: function (req, res) {
        var idImg;
        var id = dbClass.ImagesCount();
        id.then(function (e) {
            var datetimestamp = Date.now();
            //res.json({error_code:0,err_desc:null,id:e.count});
            idImg = e.count;
            dbClass.ImagesCountIncrement(e.count);
            gfs.collection('ctFiles');

            var form = new multiparty.Form();
            form.on('error', function(err){
                res.json({error_code:1,err_desc:err});
            });
            form.on('close', function() {
                console.log('finish form');
            });
            form.on('part', function(part) {
                var fileName = 'normal-' + datetimestamp + '.' +
                    part.filename.split('.')[part.filename.split('.').length -1];

                var fileNameTmb = 'tmb-' + datetimestamp + '.' +
                    part.filename.split('.')[part.filename.split('.').length -1];

                var metaData = {
                    originalname: part.filename,
                    type: 'normal',
                    id: idImg
                };
                var metaDataTmb = {
                    originalname: part.filename,
                    type: 'tmb',
                    id: idImg
                };
                    gm(part)
                        .resize('400', '400')
                        .stream(function (err, stdout, stderr) {
                            var writeStream = gfs.createWriteStream({
                                filename: fileName,
                                metadata: metaData,
                                root: "ctFiles"
                            });
                            stdout.pipe(writeStream);
                            stdout.on('error',function (err) {
                                console.log('error')
                            });
                            stdout.on('finish',function (err) {
                                console.log('finish')
                            })
                        });
                    gm(part)
                        .resize('200', '200')
                        .stream(function (err, stdout, stderr) {
                            var writeStream = gfs.createWriteStream({
                                filename: fileNameTmb,
                                metadata: metaDataTmb,
                                root: "ctFiles"
                            });
                            stdout.pipe(writeStream);
                            stdout.on('error',function (err) {
                                console.log('error')
                            });
                            // stdout.on('close',function (err) {
                            //     console.log('close')
                            // });
                            // stdout.on('finish',function (err) {
                            //     console.log('finish tmb')
                            // });
                            // writeStream.on('error',function (err) {
                            //     console.log('writeStream error')
                            // });
                            // writeStream.on('close',function (err) {
                            //     console.log('writeStream close')
                            // });
                            writeStream.on('finish',function (err) {
                                res.send({status: 'ok', text: 'Success', id: idImg});
                                console.log('writeStream finish tmb')
                            })

                        });

            });
            form.parse(req);
        });

    },
    readFile: function(req, res) {
        gfs.collection('ctFiles'); //set collection name to slookup into
        /** First check if file exists *///{filename: 'resized1.jpg'} // {metadata:{ originalname: req.params.filename}}
        var imgId = parseInt(req.params.id);
        gfs.files.find({
            'metadata.originalname':{ $exists : true },
            'metadata.type': req.params.type,
            'metadata.id':imgId
        }).toArray(function (err, files) {
            if (!files || files.length === 0) {
                return res.status(404).json({
                    responseCode: 1,
                    responseMessage: "error"
                });
            }
            /** create read stream */
            var readstream = gfs.createReadStream({
                filename: files[0].filename,
                root: "ctFiles"
            });
            /** set the proper content type */
            res.set('Content-Type', 'image/' + files[0].filename.split('.')[files[0].filename.split('.').length -1]);
            /** return response */
            return readstream.pipe(res);
        });
    }
};
