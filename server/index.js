// ======================   Подключение сторонних модулей   ======================

var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    flash = require('connect-flash'),
    fs = require('fs'),
    q = require('q');


// ======================   Подключение собственных модулей   ======================

var dbClass = require('./db/mongoDb.js'),
    logger = require('./logger/logging.js'),
    gridFs = require('./db/gridFs.js'),
    errorHandler = require('./errorHandlers/errorHandler.js'),
    mainConf = require('./config/mainConf.js'),
    router = require('./router/router.js');

// ======================   Глобальные переменный   ======================

var app = express();

module.exports = q
    .all([
        dbClass.connect(),
        gridFs.connect()
    ])
    .then(function (results) {
        console.log('Success connect to MongoDb Server');
        console.log('All connected');
        app = mainConf.configure(app);

        //=========== Генерация ошибок ==================
        // app.use('*', function (res, req, next) {
        //     var err = new Error('File not found!');
        //     err.status = 500;
        //     next(err);
        // });

        app.use(errorHandler);
        return app;
    });

