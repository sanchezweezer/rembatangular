var express = require('express'),
    multiparty = require('multiparty'),
    path = require('path'),
    fs = require('fs'),
    ejs = require('ejs'),
    dbClass = require('../db/mongoDb.js'),
    dir = require('node-dir'),
    q = require('q'),
    sendpulse = require('../mailing/smtp.js'),
    connectFlash = require('connect-flash'),
    moment = require('moment'),
    configVariables = require('../config/configVariables.js'),
    winston = require('winston');

var gridFs = require('../db/gridFs.js'),
    passport = require('../user/login.js');

var router = express.Router();

var loggerInfo = new winston.Logger({
    level: 'info',
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({filename: path.join(__dirname, '../logs/mail.log')})
    ]
});

var getIndex = function (req, res) {
    var indexData = {
        filesItems: [],
        vendorsItems: []
    };

    if(process.argv[process.argv.length-1] ==="--production"){
        indexData.env = 'production';
    }else{
        indexData.env = 'development';
    }

    var getFiles = function () {
        var deferred = q.defer();
        dir.paths(path.join(__dirname, '../../client/release/build/js'), function(err, paths) {
            if (err) throw err;
            paths.files.forEach(function (i) {
                indexData.filesItems.push(i.replace(path.join(__dirname, '../../client/release/build/js'),''));
            });
            deferred.resolve();
        });
        return deferred.promise
    };
    var filesPromise = getFiles();
    var getVendors = function () {
        var deferred = q.defer();
        dir.paths(path.join(__dirname, '../../client/release/build/vendors'), function(err, paths) {
            if (err) throw err;
            paths.files.forEach(function (i) {
                if(i.indexOf('.js') >= 0 && i.indexOf('angular.min') < 0 && i.indexOf('angular.js') < 0 && i.indexOf('.map') < 0 )
                    indexData.vendorsItems.push(i.replace(path.join(__dirname, '../../client/release/build/vendors'),''));
            });
            indexData.vendorsItems.sort();
            deferred.resolve();
        });
        return deferred.promise
    };
    var vendorsPromise = getVendors();

    q
        .all([
            filesPromise,
            vendorsPromise,
            q.nfcall(fs.readFile, path.join(__dirname, '../../client/release/build', 'index.html'))
        ])
        .then(function (e) {
            var r = ejs.render(e[2].toString(),indexData);
            res.write(r);
            res.end();
            return true;
        })
};


router.get('/', getIndex);
// router.get('/diamond-flowers', function (req, res) {
//     res.sendFile(path.join(__dirname, '../../client/release/build', 'index.html'));
// });

var answerGetter = function answerGetter(response) {
    console.log(response);
    loggerInfo.log('info', 'Mail delivery status: ', response.result || 'error code: ' + response.error_code + ' Message:' + response.message);
};

router.post('/services/getData', function getJSONObj (req, res, next) {
    var list = [{id: 'bouquet'}, {id: 'box'}, {id: 'red'}, {id: 'blue'}, {id: 'iron'}];

    var result = req.body.id ? list[req.body.id] : list;

    res.type('json');
    res.send(result);
});

router.post('/services/login', passport.authenticate('local',
    {
        successFlash: true,
        failureFlash: true
    }), function (req, res) {
        var result = req.flash().success[0];
        if (result === 'true') {
            res.json(true)
        } else {
            res.json(false);
        }
        //res.json(req.flash().success);
});

router.post('/services/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

router.post('/services/isAuth', function isAuthenticated (req, res) {
    res.type('json');
    res.send({isAuthenticated: req.isAuthenticated()});
});

router.use('/file/:type/:id', gridFs.readFile);
router.post('/image/upload', gridFs.uploadImg);

router.post('/services/getProducts', function (req, res) {
    if (req.isAuthenticated()) {
        req.body.filter = req.body.filter;
    } else {
        req.body.filter.isDeleted = false;
    }
    q.all([
        dbClass.ProductCount(),
        dbClass.ShowCollection(
            configVariables.mondgo.collections.product,
            req.body.filter || {},
            req.body.page.startTo - 1 || (req.body.page.index ? ((req.body.page.index - 1) * (req.body.page.count)) :  0),
            req.body.page.count
        )
    ])
        .then(function (e) {
        res.json({
            count: e[0],
            items: e[1]
        });
    });
});

router.post('/services/getOrders', function (req, res) {
    dbClass.ShowCollection('orders',
        req.body.filter || {},
        req.body.page.startTo - 1 || (req.body.page.index ? ((req.body.page.index - 1) * (req.body.page.count)) :  0),
        req.body.page.count
    )
        .then(function (promise) {
        //promise = promise.reverse();
        res.json(promise);
    });
});


router.post('/services/addProduct', function (req, res) {
    if (req.body.product.Id) {
        dbClass.ProductUpdate(req.body.product).then(function (response) {
                console.log(response);
                if (response.result.ok == 1 && response.result.nModified > 0 && response.result.n == 1) {
                    res.json({result: true});
                } else {
                    res.json({result: false});
                }
            });
    } else {
        dbClass.ProductCount().then(function (response) {
            req.body.product.Id = response + 1;
            req.body.product.Date = moment().format('HH:mm DD/MM/YYYY');
            console.log(req.body.product.Id);
            req.body.product.isDeleted = false;
            dbClass.InsertCollection(configVariables.mondgo.collections.product, req.body.product)
                .then(function (response) {
                    console.log(response.result);
                    if(response.result.ok == 1 && response.result.n == 1){
                        res.send({result: response.ops[0]})
                    }else{
                        res.send({result: false})
                    }
                });
        })
    }
});


router.post('/services/deleteItem/:itemId', function (req, res) {
    res.json(dbClass.DeleteItem({ id: req.params.itemId }, req.body.isDeleted));
});

router.post('/services/addOrder', function (req, res) {
        var order = {
            recipientInfo: req.body.recipientInfo,
            product: req.body.product,
            date: moment().format('HH:mm DD/MM/YYYY')
        };
        dbClass.OrdersCount().then(function (response) {
            order.number = response + 1;
            dbClass.InsertCollection(configVariables.mondgo.collections.orders, { order: order });
            if ( order.recipientInfo.email != undefined ) {
                ejs.renderFile(path.join(__dirname, '../mailing/mailingLayout.ejs'),
                    {order: order},
                    {},
                    function (err, temp) {
                        if (err) {
                            console.log(err);
                        } else {
                            sendpulse.smtpSendMail(answerGetter, {
                                "html": temp,
                                "text": "",
                                "subject": "Example subject",
                                "from": {
                                    "name": "Diamond",
                                    "email": "info@diamondflowers.ru"
                                },
                                "to": [
                                    {
                                        "name": order.recipientInfo.name,
                                        "email": order.recipientInfo.email
                                    }
                                ]
                            });
                        }
                    });
            }
        });
});

router.use('*', getIndex);
//router.post('/services/addOrder', function (req, res) {
//    res.json()
//})
// router.post('/image/upload', function(req, res, next) {
//     // создаем форму
//     var form = new multiparty.Form();
//     //здесь будет храниться путь с загружаемому файлу, его тип и размер
//     var uploadFile = {uploadPath: '', type: '', size: 0};
//     //максимальный размер файла
//     var maxSize = 2 * 1024 * 1024; //2MB
//     //поддерживаемые типы(в данном случае это картинки формата jpeg,jpg и png)
//     var supportMimeTypes = ['image/jpg', 'image/jpeg', 'image/png'];
//     //массив с ошибками произошедшими в ходе загрузки файла
//     var errors = [];
//
//     //если произошла ошибка
//     form.on('error', function(err){
//         if(fs.existsSync(uploadFile.path)) {
//             //если загружаемый файл существует удаляем его
//             fs.unlinkSync(uploadFile.path);
//             console.log('error');
//         }
//     });
//
//     form.on('close', function() {
//         //если нет ошибок и все хорошо
//         if(errors.length == 0) {
//             //сообщаем что все хорошо
//             res.send({status: 'ok', text: 'Success'});
//         }
//         else {
//             if(fs.existsSync(uploadFile.path)) {
//                 //если загружаемый файл существует удаляем его
//                 fs.unlinkSync(uploadFile.path);
//             }
//             //сообщаем что все плохо и какие произошли ошибки
//             res.send({status: 'bad', errors: errors});
//         }
//     });
//
//     // при поступление файла
//     form.on('part', function(part) {
//         //читаем его размер в байтах
//         uploadFile.size = part.byteCount;
//         //читаем его тип
//         uploadFile.type = part.headers['content-type'];
//         //путь для сохранения файла
//         uploadFile.path = __dirname + '/../files/' + part.filename;
//
//         //проверяем размер файла, он не должен быть больше максимального размера
//         if(uploadFile.size > maxSize) {
//             errors.push('File size is ' + uploadFile.size + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
//         }
//
//         //проверяем является ли тип поддерживаемым
//         if(supportMimeTypes.indexOf(uploadFile.type) == -1) {
//             errors.push('Unsupported mimetype ' + uploadFile.type);
//         }
//
//         //если нет ошибок то создаем поток для записи файла
//         if(errors.length == 0) {
//             var out = fs.createWriteStream(uploadFile.path);
//             part.pipe(out);
//         }
//         else {
//             //пропускаем
//             //вообще здесь нужно как-то остановить загрузку и перейти к onclose
//             part.resume();
//         }
//     });
//
//     // парсим форму
//     form.parse(req);
// });

module.exports = router;
