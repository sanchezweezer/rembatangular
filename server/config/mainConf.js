var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    flash = require('connect-flash'),
    fs = require('fs'),
    //favicon = require('serve-favicon'),
    q = require('q');

var logger = require('../logger/logging.js'),
    passport = require('../user/login.js'),
    configStatic = require('./configStatic.js'),
    configVariables = require('./configVariables.js'),
    router = require('../router/router.js');

module.exports = {
    configure: function (app) {

        if(process.argv[process.argv.length-1] ==="--production"){
            configVariables.production = true;
        }


        //allow cross origin requests
        app.use(function(req, res, next) {
            res.setHeader("Access-Control-Allow-Methods", "POST, GET");
            res.header("Access-Control-Allow-Origin", "https://diamondflowers.ru");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.header("Access-Control-Allow-Credentials", true);
            next();
        });

        app = configStatic(app);

        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded( { extended: true } ));
        app.use(session({
            cookie: {
                path: '/',
                httpOnly: true,
                secure: false,
                maxAge: null
            },
            secret: 'keyboard cat',
            resave: false,
            saveUninitialized: false
        }));
        app.disable('x-powered-by');
        app.use(function (req, res, next) {
            res.removeHeader("x-powered-by");
            next();
        });

        app.use(passport.initialize());
        app.use(passport.session());
        app.use(flash());
        if(configVariables.production){
            app.use(require('prerender-node').set('prerenderToken', 'RYQKRni7diUcGj9W2VtN').set('protocol', 'https').blacklisted(['/file/tmb', '/file/normal']));
            //console.log('production server');
        }else{
            //console.log('developer server');
        }

        //app.use(favicon(path.join(__dirname, '../public/favicon.ico')));

        app.use('/', router);

        console.log('App configured success');
        return app;

    }
};