var express = require('express'),
    path = require('path');


module.exports = function (app) {
    app.use('/static', express.static(path.join(__dirname, '../../client/release/static')));
    app.use('/public', express.static(path.join(__dirname, '../public')));
    app.use('/browserconfig.xml', express.static(path.join(__dirname, '../browserconfig.xml')));
    app.use('/google8fecdde9fef96f89.html', express.static(path.join(__dirname, '../google8fecdde9fef96f89.html')));
    app.use('/sitemap.xml', express.static(path.join(__dirname, '../sitemap.xml')));
    app.use('/robots1.txt', express.static(path.join(__dirname, '../robots.txt')));
    app.use('/assets', express.static(path.join(__dirname, '../../client/release/assets')));
    app.use('/vendor', express.static(path.join(__dirname, '../../client/release/build/vendors')));
    app.use('/spa', express.static(path.join(__dirname, '../../client/release/build')));
    return app;
};